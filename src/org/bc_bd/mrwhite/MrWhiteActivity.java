/*
 * Copyright (c) 2012,2013 Stefan Völkel <bd@bc-bd.org>
 *
 * Released under the GPL v2. See file License for details.
 *
 * */

package org.bc_bd.mrwhite;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

public class MrWhiteActivity extends Activity {
	private final static String TAG = "MrWhiteActivity";

	private static final int step = 10;

	View view_;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		view_ = new View(this);
		view_.setKeepScreenOn(true);
		applySettings();
		setContentView(view_);
	}

	@Override
	public void onStart() {
		super.onStart();

		applySettings();
	}

	public void applySettings() {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);

		WindowManager.LayoutParams lp = getWindow().getAttributes();
		lp.screenBrightness = getBrightness() / 100f;
		getWindow().setAttributes(lp);

		// for full screen mode
		getWindow().addFlags(LayoutParams.FLAG_FULLSCREEN);

		int color = settings.getInt("color", 0xffffffff);
		view_.setBackgroundColor(color);
	}

	private int getBrightness() {
		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(this);

		return settings.getInt("brightness", 100);
	}

	private void changeBrightness(int change) {
		int brightness = getBrightness() + change;

		if (brightness > 100)
			brightness = 100;
		if (brightness < step)
			brightness = step;

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("brightness", brightness);
		editor.commit();

		Log.d(TAG, "changed brightness to " + Integer.toString(brightness));
	}

	@Override
	public boolean dispatchKeyEvent(KeyEvent event) {
		int action = event.getAction();
		int keyCode = event.getKeyCode();

		switch (keyCode) {
		case KeyEvent.KEYCODE_VOLUME_UP:
			if (action == KeyEvent.ACTION_UP) {
				changeBrightness(step);
				applySettings();
			}
			return true;
		case KeyEvent.KEYCODE_VOLUME_DOWN:
			if (action == KeyEvent.ACTION_DOWN) {
				changeBrightness(-step);
				applySettings();
			}
			return true;
		default:
			return super.dispatchKeyEvent(event);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		Log.d(TAG, "onPrepareOptionsMenu");
		Intent settingsActivity = new Intent(getBaseContext(),
				Preferences.class);
		startActivity(settingsActivity);
		return true;
	}

}
