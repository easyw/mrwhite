package org.bc_bd.mrwhite;

import org.bc_bd.mrwhite.RGBPickerDialog.OnColorChangedListener;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.SeekBar;
import android.widget.Toast;

public class Preferences extends PreferenceActivity implements
		OnColorChangedListener {

	private final static String TAG = "Preferences";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences);

		Preference about = (Preference) findPreference("about");
		about.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			public boolean onPreferenceClick(Preference preference) {
				Log.d(TAG, "about");

				Version v = new Version(Preferences.this);
				String template = getResources().getString(
						R.string.about_dialog);
				String text = String.format(template, v.name(), v.code());

				new AlertDialog.Builder(Preferences.this)
						.setTitle(R.string.about)
						.setMessage(text)
						.setPositiveButton(android.R.string.ok,
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										// continue with delete
									}
								}).show();
				return true;
			}

		});

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		int c = settings.getInt("color", 0xffffffff);

		Preference color = (Preference) findPreference("color");

		colorSummary(c);

		color.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				SharedPreferences settings = PreferenceManager
						.getDefaultSharedPreferences(getBaseContext());
				int c = settings.getInt("color", 0xffffffff);

				RGBPickerDialog d = new RGBPickerDialog(Preferences.this,
						Preferences.this, c);
				d.show();
				return true;
			}

		});

		Preference brightness = (Preference) findPreference("brightness");
		int b = settings.getInt("brightness", 100);

		brightnessSummary(b);

		brightness
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {
					public boolean onPreferenceClick(Preference preference) {
						brightness();
						return true;
					}

				});

		Preference reset = (Preference) findPreference("reset");
		reset.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
				colorChanged(Color.WHITE);
				brightnessChanged(100);
				Toast.makeText(Preferences.this,
						getString(R.string.reset_summary), Toast.LENGTH_SHORT)
						.show();
				return true;
			}

		});
	}

	private void brightness() {
		LayoutInflater inflater = getLayoutInflater();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final View settingsView = inflater.inflate(R.layout.brightness, null);

		final SeekBar seekbar = (SeekBar) settingsView
				.findViewById(R.id.brightness);

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		int brightness = settings.getInt("brightness", 100);

		seekbar.setProgress(brightness);

		builder.setTitle(R.string.brightness);
		builder.setView(settingsView);
		builder.setPositiveButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						brightnessChanged(seekbar.getProgress());
					}
				});

		builder.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					}
				});

		AlertDialog dialog = builder.create();
		dialog.show();
	}

	private void colorSummary(int c) {
		Preference color = (Preference) findPreference("color");

		String template = getResources().getString(R.string.color_summary);
		color.setSummary(String.format(template, c));
	}

	private void brightnessSummary(int b) {
		Preference brightness = (Preference) findPreference("brightness");

		String template = getResources().getString(R.string.brightness_summary);
		brightness.setSummary(String.format(template, b));
	}

	public void colorChanged(int color) {
		Log.d(TAG, "color changed to " + Integer.toString(color));

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("color", color);
		editor.commit();

		colorSummary(color);
	}

	public void brightnessChanged(int brightness) {
		Log.d(TAG, "brightness changed to " + Integer.toString(brightness));

		if (brightness <= 0)
			brightness = 1;

		SharedPreferences settings = PreferenceManager
				.getDefaultSharedPreferences(getBaseContext());
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt("brightness", brightness);
		editor.commit();

		brightnessSummary(brightness);
	}

}
