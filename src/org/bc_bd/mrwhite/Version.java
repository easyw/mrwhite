package org.bc_bd.mrwhite;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;

public class Version {
	private final static String TAG = "Version";

	private int versionCode_ = 0;
	private String versionName_ = "unknown";

	Version(Context c) {
		PackageInfo pi;

		try {
			pi = c.getPackageManager().getPackageInfo(c.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			Log.e(TAG, "getVersionInormation()", e);
			return;
		}

		versionCode_ = pi.versionCode;
		versionName_ = pi.versionName;
	}

	public int code() {
		return versionCode_;
	}

	public String name() {
		return versionName_;
	}
}
