package org.bc_bd.mrwhite;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class RGBPickerDialog extends Dialog {
	public interface OnColorChangedListener {
		void colorChanged(int color);
	}

	private OnColorChangedListener listener_;
	private int color_;

	private static class ColorPickerView extends View {
		private final static String TAG = "ColorPickerView";

		private OnColorChangedListener listener_;
		Paint paint_;
		int color_;
		GradientDrawable blue_;

		ColorPickerView(Context c, OnColorChangedListener l, int color) {
			super(c);

			listener_ = l;
			color_ = color;
			paint_ = new Paint(Paint.ANTI_ALIAS_FLAG);

			blue_ = new GradientDrawable(Orientation.LEFT_RIGHT, new int[] {
					Color.BLACK, Color.RED });
			blue_.setBounds(10, 10, 10 + 255, 10 + 40);
		}

		@SuppressLint("DrawAllocation")
		@Override
		protected void onDraw(Canvas canvas) {
			Log.d(TAG, "onDraw");

			int r = Color.red(color_);
			int g = Color.green(color_);
			int b = Color.blue(color_);

			paint_.setStyle(Paint.Style.FILL);

			blue_.draw(canvas);

			for (int i = 0; i <= 255; ++i) {
				int from = Color.rgb(r, i, 0);
				int to = Color.rgb(r, i, 255);

				LinearGradient gb = new LinearGradient(10 + i, 10 + 40 + 10,
						10 + i, 10 + 40 + 10 + 255, from, to,
						Shader.TileMode.CLAMP);
				paint_.setShader(gb);

				canvas.drawLine(10 + i, 10 + 40 + 10, 10 + i,
						10 + 40 + 10 + 255, paint_);
			}

			paint_.setShader(null);
			paint_.setColor(color_);
			canvas.drawRect(10, 10 + 40 + 10 + 255 + 10, 10 + 255, 10 + 40 + 10
					+ 255 + 10 + 40, paint_);

			paint_.setStyle(Paint.Style.STROKE);
			paint_.setColor(Color.WHITE);
			paint_.setStrokeWidth(3);

			canvas.drawCircle(10 + g, 10 + 40 + 10 + b, 10, paint_);

			canvas.drawRect(10 - 3 + r, 10 - 1, 10 + 3 + r, 10 + 40 + 1, paint_);
		}

		@Override
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
			int width = 10 + 256 + 10;
			int height = 10 + 256 + 10 + 40 + 10 + 40 + 10;
			setMeasuredDimension(width, height);
		}

		@Override
		public boolean onTouchEvent(MotionEvent event) {
			float x = event.getX();
			float y = event.getY();

			if (x > 10 && x < 10 + 255 && y > 10 && y < 10 + 40) {
				Log.d(TAG, "inside box");

				int g = Color.green(color_);
				int b = Color.blue(color_);

				color_ = Color.rgb((int) (x - 10), g, b);

				invalidate();
			}

			if (x > 10 && x < 255 + 10 && y > 10 + 40 + 10
					&& y < 10 + 40 + 10 + 255) {
				Log.d(TAG, "inside square");

				int r = Color.red(color_);

				color_ = Color.rgb(r, (int) (x - 10), (int) (y - 10 - 40 - 10));

				invalidate();
			}

			if (x > 10 && x < 10 + 255 && y > 10 + 40 + 10 + 255 + 10
					&& y < 10 + 40 + 10 + 255 + 10 + 40)
				listener_.colorChanged(color_);

			return true;
		}
	}

	public RGBPickerDialog(Context context, OnColorChangedListener listener,
			int color) {
		super(context);

		listener_ = listener;
		color_ = color;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		OnColorChangedListener l = new OnColorChangedListener() {
			public void colorChanged(int color) {
				listener_.colorChanged(color);
				dismiss();
			}
		};

		setContentView(new ColorPickerView(getContext(), l, color_));
		setTitle(R.string.color);
	}
}
